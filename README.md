**CREATING USER:**
curl -X POST http://localhost:8081/create -H "Content-Type: application/json" -d """{"id":6,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Simply user"],"created":true,"hash":1245231,"timeout":15}"""

| Answers                                                  |
-----------------------------------------------------------|
| {"code":200,"message":"Success"}                         |
| Send message with code what equal produced error	   |


**UPDATING USER'S INFORMATION:**
curl -X POST http://localhost:8081/update -H "Content-Type: application/json" -d """{"id":7,"enabled":false,"login":"SomeChangedLogin","email":"someemailafterupgrade.com","name":"SomeNameUpgraded","surname":"SomeSurnameUpgraded","roles":["SUPER ADMIN"],"groups":["78773as2123fj234"],"permissions":["Nothing as Nothing"],"info":["Simply as updated user"],"created":true,"hash":124225231,"timeout":15}"""

| Answers                                                    |
-------------------------------------------------------------|
| {"code":200,"message":"Success"}                           |
|  Send message with code what equeal produced error	     |

**UPDATING PASSWORD:**
curl -X POST http://localhost:8081/update_password -H "Content-Type: application/json" -d """{""id"": 7, ""hash"":123456789104564646}"""

| Answers                                                    |
-------------------------------------------------------------|
| {"code":200,"message":"Success"}                           |
|  Send message with code what equal produced error	     |


**UPDATING PASSWORD BY USER:**
curl -X POST http://localhost:8081/profile/update_password -H "Content-Type: application/json" -d """{""login"":""SomeLogin"", ""oldPassword"":123456789, ""newPassword"":1092388848484888}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
|  Send message with code what equal produced error	       |
                 

**UPDATING USER'S INFORMATION BY USER:**
curl -X POST http://localhost:8081/profile/update_info -H "Content-Type: application/json" -d """{"id":7,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Very powerful user"],"created":true,"hash":1245231,"timeout":15}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |

**ENABLE USER:**
curl -X POST http://localhost:8081/enable -H "Content-Type: application/json" -d """{""id"":7}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |


**DISABLE USER:**
curl -X POST http://localhost:8081/disable -H "Content-Type: application/json" -d "{""id"":7}"

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |


**COMPLETE LIST OF USERS WITH THE ABILITY TO FILTER THEM:**
curl -X POST http://localhost:8081/list -H "Content-Type: application/json" -d """{""id"": , ""enabled"":"""",""login"":"""", ""email"":"""", ""name"":"""", ""surname"":"""", ""roles"":[], ""groups"":[], ""permissions"":[], ""info"":[], ""created"":[], ""timeout"": }"""
                                  
| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":["""{"id":7,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Simply user"],"created":true,"hash":1245231,"timeout":15}"""}|
| Send message with code what equeal produced error            |

**LIST OF ACCOUNT PERMISSIONS:**
curl -X POST http://localhost:8081/permissions -H "Content-Type: application/json" 

| Answers                                                       |
----------------------------------------------------------------|
| {"provide":["Nothing"]}                                       |
| Send message with code what equal produced error              |

**SESSION TIME OF ACCOUNT:**
curl -X POST http://localhost:8081/session_time -H "Content-Type: application/json" -d """{""login"":""SomeLogin""}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"15"}                                  |
| Send message with code what equeal produced error            |


**DELETE ROLE:**
curl -X POST http://localhost:8081/delete_role -H "Content-Type: application/json" -d """{""id"":7}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |

**CREATE ROLE**:
curl -X POST http://localhost:8081/role_create -H "Content-Type: application/json" -d """{"id":7,""name"":""Admins"",""description"":""Some roles of admin"",""permissions"":[""Everything""]}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |

**DELETE GROUP:**
curl -X POST http://localhost:8081/delete_group -H "Content-Type: application/json" -d """{"deleteId":"78773as2123fj2","relocatedId":"123sk23dmz794"}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":"Success"}                             |
| Send message with code what equal produced error             |


**LIST OF ALL PERMISSIONS:**
curl -X POST http://localhost:8081/permissions -H "Content-Type: application/json" -d """{"id":7}"""

| Answers                                                      |
---------------------------------------------------------------|
| {"code":200,"message":[{["Nothing"]}]                        | 
| Send message with code what equal produced error             |


