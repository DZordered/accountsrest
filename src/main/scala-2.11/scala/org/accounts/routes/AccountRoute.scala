package scala.org.accounts.routes


import spray.httpx.UnsuccessfulResponseException
import spray.httpx.marshalling._
import spray.routing.HttpService

import scala.org.accounts.utils.MasterJsonProtocol._
import scala.org.accounts.utils._





trait AccountRoute extends HttpService {
  val routs =
    path("create") {
      post {
        entity(as[User]) { user =>
          try {
            CustomMongoBuilder.mongoObjectCreate(user)
          } catch {
            case responseEx: UnsuccessfulResponseException =>
              requestUri {
                uri => complete(responseEx.response.status)
              }
          }
          complete {
            marshal(Response(code = 200, message = "Success"))
          }
        }
      }
    }~
          path("update") {
            post {
              entity(as[User]) { user =>
                try {
                  CustomMongoBuilder.mongoObjectUpdate(user)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete {
                  marshal(Response(code = 200, message = "Success"))
                }
              }
            }
          }~
          path("update_password") {
            post {
              entity(as[UpdatePass]) { pass =>
                try {
                  CustomMongoBuilder.updatePass(pass)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete {
                  marshal(Response(code = 200, message = "Success"))
                }
              }
            }
          }~
          path("profile" / "update_password") {
            post {
              entity(as[UpdatePassFromUser]) { uppass =>
                try {
                  CustomMongoBuilder.updatePassByUser(uppass)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete {
                  marshal(Response(code = 200, message = "Success"))
                }
              }
            }
          }~
          path("profile" / "update_info") {
            post {
              entity(as[User]) { user =>
                try {
                  CustomMongoBuilder.updateInfoByUser(user)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete {
                  marshal(Response(code = 200, message = "Success"))
                }
              }
            }
          }~
          path("list") {
            post {
              entity(as[User]) { user =>
                var userList: List[User] = List[User]()
                try {
                  userList = CustomMongoBuilder.getListOfUsers(user)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete(userList)
              }
            }
          }~
          path("enable") {
            post {
              entity(as[Id]) { id =>
                try {
                  CustomMongoBuilder.enableUser(id)
                } catch {
                  case responseEx: UnsuccessfulResponseException =>
                    requestUri {
                      uri => complete(responseEx.response.status)
                    }
                }
                complete {
                  marshal(Response(code = 200, message = "Success"))
                }
            }
          }
          }~
        path("disable") {
          post {
            entity(as[Id]) { id =>
              try {
                CustomMongoBuilder.disableUser(id)
              } catch {
                case responseEx: UnsuccessfulResponseException =>
                  requestUri {
                    uri => complete(responseEx.response.status)
                  }
              }
              complete {
                marshal(Response(code = 200, message = "Success"))
              }
            }
          }
        }~
            path("delete_group") {
              post {
                entity(as[GroupDelete]) { deleteForm =>
                  try {
                    CustomMongoBuilder.deleteGroupById(deleteForm)
                  } catch {
                    case responseEx: UnsuccessfulResponseException =>
                      requestUri {
                        uri => complete(responseEx.response.status)
                      }
                  }
                  complete {
                    marshal(Response(code = 200, message = "Success"))
                  }
                }
              }
            }~
              path("delete_role") {
                post {
                  entity(as[Id]) { id =>
                    try {
                      CustomMongoBuilder.deleteRoleById(id)
                    } catch {
                      case responseEx: UnsuccessfulResponseException =>
                        requestUri {
                          uri => complete(responseEx.response.status)
                        }
                    }
                    complete {
                      marshal(Response(code = 200, message = "Success"))
                    }
                  }
                }
              }~
                  path("insert_role") {
                    post {
                      entity(as[Role]) { role =>
                        try {
                          CustomMongoBuilder.createRole(role)
                        } catch {
                          case responseEx: UnsuccessfulResponseException =>
                            requestUri {
                              uri => complete(responseEx.response.status)
                            }
                        }
                        complete {
                          marshal(Response(code = 200, message = "Success"))
                        }
                      }
                    }
                  }~
                  path("session_time") {
                    post {
                      entity(as[Login]) { login =>
                        var session: Int = 0
                        try {
                          session = CustomMongoBuilder.getSessionTime(login.login)
                        } catch {
                          case responseEx: UnsuccessfulResponseException =>
                            requestUri {
                              uri => complete(responseEx.response.status)
                            }
                        }
                        complete {
                          marshal(session.toString)
                        }
                      }
                    }
                  }~
                  path("permissions") {
                    post {
                      entity(as[Id]) { id =>
                        var permission: List[String] = List[String]()
                        try {
                          permission = CustomMongoBuilder.getPermissionsById(id.id)
                        } catch {
                          case responseEx: UnsuccessfulResponseException =>
                            requestUri {
                              uri => complete(responseEx.response.status)
                            }
                        }
                        complete {
                          marshal(permission)
                        }
                      }
                    }
                  }

}
