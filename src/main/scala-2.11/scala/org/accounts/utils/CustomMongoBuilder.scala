package scala.org.accounts.utils

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.core.util.StatusPrinter
import org.slf4j.LoggerFactory
import reactivemongo.api.ReadPreference
import reactivemongo.bson._
import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.org.accounts.MongoDBConnection
import scala.util.{Failure, Success}

object MasterJsonProtocol extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val userFormat = jsonFormat13(User)
  implicit val updatePass = jsonFormat2(UpdatePass)
  implicit val updatePassFromUser = jsonFormat3(UpdatePassFromUser)
  implicit val response = jsonFormat2(Response)
  implicit val groupDelete = jsonFormat2(GroupDelete)
  implicit val role = jsonFormat4(Role)
  implicit val id = jsonFormat1(Id)
  implicit val login = jsonFormat1(Login)
  implicit val session = jsonFormat1(Session)
}
case class User(  id: Int,
                 enabled: Boolean,
                login: String,
                email: String,
                name: String,
                surname: String,
                roles: List[String],
                groups: List[String],
                permissions: List[String],
                info: List[String],
                created: Boolean,
                hash: Int,
                timeout: Int)
case class Login(login: String)
case class UpdatePass(id: Int, hash: Int)
case class Id(id : Int)
case class UpdatePassFromUser(login: String, oldPassword: Int, newPassword: Int)
case class Response(code: Int = 200, message: String = "Success")
case class GroupDelete(deleteId: String, relocatedId: String)
case class Role(id: Int, name: String, description: String, permissions: List[String])
case class Session(session: Int)

object CustomLogger {
  def logger = LoggerFactory.getLogger("AccountsRESTLogging")
  StatusPrinter.print(LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext])
}
object CustomMongoBuilder {
  def mongoObjectCreate(user: User) : Boolean = {
    if(user == null) throw new NullPointerException

    val randId = user.id + 1

    val doc = BSONDocument(
      "id" -> randId,
      "enabled" -> user.enabled,
      "login" -> user.login,
      "email" -> user.email,
      "name" -> user.name,
      "surname" -> user.surname,
      "roles" -> user.roles,
      "permissions" -> user.permissions,
      "info" -> user.info,
      "created" -> user.created,
      "hash" -> user.hash,
      "timeout" -> user.timeout
    )

        MongoDBConnection.collection_accounts.insert(doc).onComplete{
        case Success(writeResult) => CustomLogger.logger.info("User created successfully")
        case Failure(exception) => throw exception
       }
    true
  }

  def mongoObjectUpdate(user: User) : Boolean = {
    if(user == null) throw new NullPointerException

    val query = BSONDocument(
      "id" -> user.id
    )

    val mod = BSONDocument(
            "$set" -> BSONDocument(
              "id" -> user.id,
              "enabled" -> user.enabled,
              "login" -> user.login,
              "email" -> user.email,
              "name" -> user.name,
              "surname" -> user.surname,
              "roles" -> user.roles,
              "permissions" -> user.permissions,
              "info" -> user.info,
              "created" -> user.created,
              "hash" -> user.hash,
              "timeout" -> user.timeout
            )
    )
    MongoDBConnection.collection_accounts.update(query, mod).onComplete {
      case Success(updated) => CustomLogger.logger.info("User updated successfully")
      case Failure(ex) => throw ex
    }
    true
  }
  def updatePass(updatePass: UpdatePass) : Boolean = {
    if(updatePass == null) throw new NullPointerException
    implicit val reader = Macros.reader[UpdatePass]

    MongoDBConnection.collection_accounts.findAndUpdate(
        BSONDocument("id" -> updatePass.id),
          BSONDocument("$set" -> BSONDocument("hash" -> updatePass.hash))
    ).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("Password updated successfully")
      case Failure(ex) => throw ex
    }
    true
  }

  def updatePassByUser(updatePassFromUser: UpdatePassFromUser): Boolean = {
    if(updatePassFromUser == null) throw new NullPointerException
    implicit val reader = Macros.reader[UpdatePassFromUser]

    MongoDBConnection.collection_accounts.findAndUpdate(
      BSONDocument("login" -> updatePassFromUser.login),
        BSONDocument("$set" -> BSONDocument("hash" -> updatePassFromUser.newPassword))
    ).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("Password by user updated successfully")
      case Failure(ex) => throw ex
    }
  true
  }

  def updateInfoByUser(user: User): Boolean = {
    if(user == null) throw new NullPointerException
    implicit val reader = Macros.reader[User]

    MongoDBConnection.collection_accounts.findAndUpdate(
        BSONDocument("login" -> user.login),
        BSONDocument("$set" -> BSONDocument("info" -> user.info))
    ).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("Information updated successfully")
      case Failure(ex) => throw ex
    }
    true
  }
  def enableUser(idFromRequest: Id) : Boolean = {
    if(idFromRequest == null) throw new NullPointerException
    implicit val reader = Macros.reader[User]

    MongoDBConnection.collection_accounts.findAndUpdate(
      BSONDocument("id" -> idFromRequest.id),
        BSONDocument("$set" -> BSONDocument("enabled" -> true))
    ).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("User enabled successfully")
      case Failure(ex) => throw ex
    }
    true
  }

  def disableUser(idFromRequest: Id) = {
    if(idFromRequest == null) throw new NullPointerException
    implicit val reader = Macros.reader[User]

    MongoDBConnection.collection_accounts.findAndUpdate(
      BSONDocument("id" -> idFromRequest.id),
        BSONDocument("$set" -> BSONDocument("enabled" -> false))
    ).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("User disabled successfully")
      case Failure(ex) => throw ex
    }
  }

  def deleteGroupById(groupDelete: GroupDelete) = {
    if(groupDelete == null) throw new NullPointerException
      implicit val reader = Macros.reader[GroupDelete]
      val select = BSONDocument("groups" -> BSONArray(groupDelete.deleteId))
      val mod = BSONDocument("$set" -> List(groupDelete.relocatedId))
      MongoDBConnection.collection_accounts.findAndUpdate(select, mod).onComplete {
        case Success(passUpdated) => CustomLogger.logger.info("Group is deleted successfully")
        case Failure(ex) => throw ex
      }
  }

  def deleteRoleById(id: Id) = {
    if(id == null) throw new NullPointerException
    implicit val reader = Macros.reader[User]
    val select = BSONDocument("id" -> id.id)
    val mod = BSONDocument("$set" -> BSONDocument("roles" -> List[String]()))
    MongoDBConnection.collection_accounts.findAndUpdate(select, mod).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("Role is deleted successfully")
      case Failure(ex) => throw ex
    }
  }

  def createRole(role: Role) = {
    if(role == null) throw new NullPointerException
    implicit val reader = Macros.reader[User]
    val select = BSONDocument("id" -> role.id)
    val mod = BSONDocument("$set" ->
      BSONDocument("roles" -> List[String](role.name + " " + role.description + " " + role.permissions )))

    MongoDBConnection.collection_accounts.findAndUpdate(select, mod).onComplete {
      case Success(passUpdated) => CustomLogger.logger.info("Role is created successfully")
      case Failure(ex) => throw ex
    }
  }

  def getSessionTime(login: String) : Int = {
    if(login == null) throw new NullPointerException
    var sessionTimeAsInt: Int = 0

    MongoDBConnection.collection_accounts
      .find(BSONDocument("login" -> login))
      .one[BSONDocument](ReadPreference.secondaryPreferred)
      .map(doc => doc.get.getAs[Int]("timeout").get)
      .onComplete {
      case Success(doc) =>
        CustomLogger.logger.info("Session time is get successfully")
        sessionTimeAsInt = doc
      case Failure(ex) => ex.getStackTrace
    }
    sessionTimeAsInt
  }

  def getPermissionsById(id : Int): List[String] = {
    if(id == 0) throw new IllegalArgumentException
    var listOfPermissions = List[String]()
    MongoDBConnection.collection_accounts
      .find(BSONDocument("id" -> id))
      .one[BSONDocument](ReadPreference.secondaryPreferred)
      .map {
        doc => doc.get.getAs[List[String]]("permissions").get
      } onComplete {
      case Success(value) =>
        CustomLogger.logger.info("List of permissions created")
        listOfPermissions = value
      case Failure(ex) => ex.printStackTrace()
      }
   listOfPermissions
  }

  def getListOfUsers(user: User) : List[User] = {
    var listOfUsers : mutable.MutableList[User] = mutable.MutableList[User]()
   MongoDBConnection.collection_accounts
      .find(BSONDocument("timeout" -> 15))
      .cursor[BSONDocument](ReadPreference.secondaryPreferred)
      .collect[List]()
      .map {
        docL => docL.foreach(bsdoc =>listOfUsers += read(bsdoc))
      }.onComplete {
     case Success(s) => CustomLogger.logger.info("List of users created")
     case Failure(ex) => ex.printStackTrace()
   }
    listOfUsers.toList
  }

  def read(document: BSONDocument) : User = {
    if (document == null) throw new NullPointerException
    User(
      document.getAs[Int]("id").getOrElse(default = 0),
      document.getAs[Boolean]("enabled").getOrElse(default = false),
      document.getAs[String]("login").getOrElse(default = "None"),
      document.getAs[String]("email").getOrElse(default = "None"),
      document.getAs[String]("name").getOrElse(default = "None"),
      document.getAs[String]("surname").getOrElse("None"),
      document.getAs[List[String]]("roles").getOrElse(List[String]()),
      document.getAs[List[String]]("groups").getOrElse(List[String]()),
      document.getAs[List[String]]("permissions").getOrElse(List[String]()),
      document.getAs[List[String]]("info").getOrElse(List[String]()),
      document.getAs[Boolean]("created").getOrElse(false),
      document.getAs[Int]("hash").getOrElse(0),
      document.getAs[Int]("timeout").getOrElse(15)
    )
  }

}
