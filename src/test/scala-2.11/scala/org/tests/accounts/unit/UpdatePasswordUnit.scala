package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, UpdatePass}

class UpdatePasswordUnit extends FunSuite with ScalaFutures {

  test("Update pass"){
    assert(CustomMongoBuilder.updatePass(UpdatePass(id = 9684, hash = 2123412313)))
  }
}