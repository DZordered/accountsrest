package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, UpdatePassFromUser}

class UpdatePasswordByUserUnit extends FunSuite with ScalaFutures {


  test("Update info by user"){
    assert(CustomMongoBuilder.updatePassByUser(UpdatePassFromUser(login = "somelogin"
      ,oldPassword = 1111, newPassword = 2222)))
  }

  test("Pass null into update info"){
    intercept[NullPointerException]{
      CustomMongoBuilder.updatePassByUser(UpdatePassFromUser(null,0,0))
    }
  }
}