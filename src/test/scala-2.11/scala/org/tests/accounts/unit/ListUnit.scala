package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, User}

class ListUnit extends FunSuite with ScalaFutures {

  test("get list with users (filtered)"){
    assert(List[User]() == CustomMongoBuilder.getListOfUsers(null).toList)
  }

}