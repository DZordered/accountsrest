package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, Login}

class SessionTimeUnit extends FunSuite with ScalaFutures {

  test("get session time by login"){
    assert(CustomMongoBuilder.getSessionTime(Login(login = "SomeLogin").login) == 0)
  }

  test("Pass null parameter into"){
    intercept[NullPointerException]{
      CustomMongoBuilder.getSessionTime(null)
    }
  }
}