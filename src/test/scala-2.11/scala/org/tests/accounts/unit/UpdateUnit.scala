package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, User}

class UpdateUnit extends FunSuite with ScalaFutures {

  val groups = List[String]()
  val roles = List[String]()
  val infos = List[String]()
  val permissions = List[String]()
  val user = User(
    1,
    enabled = true,
    "login",
    "email",
    "name",
    "surname",
    roles.asInstanceOf[List[String]],
    groups.asInstanceOf[List[String]],
    permissions.asInstanceOf[List[String]],
    infos.asInstanceOf[List[String]],
    created = true,
    1111,
    15)

  test("Update user in collection"){
    assert(CustomMongoBuilder.mongoObjectUpdate(user))
  }
  test("Pass null into"){
   intercept[NullPointerException] {
     CustomMongoBuilder.mongoObjectUpdate(null)
   }
  }
}