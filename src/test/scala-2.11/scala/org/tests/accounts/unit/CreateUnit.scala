package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, User}

class CreateUnit extends FunSuite with ScalaFutures {

  val groups = List[String]()
  val roles = List[String]()
  val infos = List[String]()
  val permissions = List[String]()
  val user = User(
    1,
    enabled = true,
    "login",
    "email",
    "name",
    "surname",
    roles.asInstanceOf[List[String]],
    groups.asInstanceOf[List[String]],
    permissions.asInstanceOf[List[String]],
    infos.asInstanceOf[List[String]],
  created = true,
  1111,
  15)

  test("insert user in collection"){
    assert(CustomMongoBuilder.mongoObjectCreate(user))
  }

  test("insert null in collection"){
    intercept[NullPointerException]{
      CustomMongoBuilder.mongoObjectCreate(null)
    }
  }
}