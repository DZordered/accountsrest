package scala.org.tests.accounts.unit

import org.scalatest.FunSuite
import org.scalatest.concurrent.ScalaFutures

import scala.org.accounts.utils.{CustomMongoBuilder, Id}

class EnableUnit extends FunSuite with ScalaFutures {

  val id = Id(10)
  test("enable user"){
      assert(CustomMongoBuilder.enableUser(id))
  }

  test("try to enable user with null argument"){
    intercept[NullPointerException]{
      CustomMongoBuilder.enableUser(null)
    }
  }
}