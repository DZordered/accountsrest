package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute

class SessionTimeSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/session_time"

  it should "return session time in JSON response" in {
    Post(url, HttpEntity(`application/json`, """{"login":"SomeLogin"}""")) ~> routs ~> check {
      response.entity === 15.toString
    }
  }
}