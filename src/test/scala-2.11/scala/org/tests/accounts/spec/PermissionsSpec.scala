package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute

class PermissionsSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/permissions"

  it should "return permission list as JSON" in {
    Post(url, HttpEntity(`application/json`, """{"id":3}""")) ~> routs ~> check {
      response.entity === List[String]("Nothing")
    }
  }

}