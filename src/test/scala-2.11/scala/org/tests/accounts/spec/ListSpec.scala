package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import spray.http.ContentTypes._

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.User

class ListSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/list"

  it should "return list as JSONArray with users (filtered)" in {
    Post(url, HttpEntity(`application/json`, """{"id":3,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Simply user"],"created":true,"hash":1245231,"timeout":15}""")) ~> routs ~> check {
     response.entity === List[User]().nonEmpty
    }
  }
}