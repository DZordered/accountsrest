package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class CreateSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  var actorRefFactory = system
  val url ="/create"
  it should "return JSON if user created" in {
    Post(url, HttpEntity(`application/json`, """{"id":2,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Simply user"],"created":true,"hash":1245231,"timeout":15}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }



}