package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class ProfileUpdateInfoSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/profile/update_info"

  it should "return JSON response if user successfully updated info by user" in {
    Post(url, HttpEntity(`application/json`, """{"id":3,"enabled":true,"login":"SomeLogin","email":"someemail.com","name":"SomeName","surname":"SomeSurname","roles":["User"],"groups":["78773as2123fj2"],"permissions":["Nothing"],"info":["Very powerful user"],"created":true,"hash":1245231,"timeout":15}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}