package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class DeleteGroupSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/delete_group"

  it should "return JSON if user group successfully deleted" in {
    Post(url, HttpEntity(`application/json`, """{"deleteId":"78773as2123fj2","relocatedId":"123sk23dmz794"}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}