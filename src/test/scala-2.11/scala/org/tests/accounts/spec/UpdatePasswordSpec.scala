package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class UpdatePasswordSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/update_password"

  it should "return JSON response if password updated" in {
    Post(url, HttpEntity(`application/json`, """{"id":3,"hash": 1245231}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}