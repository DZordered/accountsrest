package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class UpdateSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  def actorRefFactory = system
  val url = "/update"

  it should "return JSON response if user successfully updated" in {
    Post(url, HttpEntity(`application/json`, """{"id":3,"enabled":false,"login":"SomeChangedLogin","email":"someemailafterupgrade.com","name":"SomeNameUpgraded","surname":"SomeSurnameUpgraded","roles":["SUPER ADMIN"],"groups":["78773as2123fj234"],"permissions":["Nothing as Nothing"],"info":["Simply as updated user"],"created":true,"hash":124225231,"timeout":15}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}