package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response


class RoleDeleteSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  var actorRefFactory = system
  val url ="/delete_role"
  it should "return JSON if user created" in {
    Post(url, HttpEntity(`application/json`, """{"id":3}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}