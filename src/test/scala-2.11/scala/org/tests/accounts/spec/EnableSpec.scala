package scala.org.tests.accounts.spec

import org.scalatest.FlatSpec
import org.scalatest.concurrent.ScalaFutures
import spray.http.ContentTypes._
import spray.http.HttpEntity
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.org.accounts.routes.AccountRoute
import scala.org.accounts.utils.Response

class EnableSpec extends FlatSpec with ScalatestRouteTest with HttpService with AccountRoute with ScalaFutures {
  val url = "/enable"

  def actorRefFactory = system

  it should "return JSON if user enabled successfully" in {
    Post(url, HttpEntity(`application/json`, """{"id":3}""")) ~> routs ~> check {
      response.entity === Response(code = 200, message = "Success")
    }
  }
}